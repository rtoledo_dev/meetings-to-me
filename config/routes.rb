# frozen_string_literal: true

require 'sidekiq/web'
Rails.application.routes.draw do
  mount Sidekiq::Web => "/sidekiq"

  devise_for :users

  root 'meetings#index'
  resources :rooms
  resources :meetings

end
