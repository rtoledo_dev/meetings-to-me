# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module MeetingsToMe
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults(6.1)
    config.time_zone = 'Brasilia'
    config.active_record.default_timezone = :local

    # Don't generate system test files.
    config.generators.system_tests = nil
  end
end
