FROM ruby:2.7.2
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs postgresql-client yarn
RUN mkdir /usr/src/meetings-to-me
WORKDIR /usr/src/meetings-to-me
ADD . /usr/src/meetings-to-me
RUN yarn install --check-files
RUN gem install bundler -v 2.0.1
RUN bundle install
