# frozen_string_literal: true

class Room < ApplicationRecord
  default_scope { order(created_at: :desc) }
  has_many :meetings, autosave: true, dependent: :destroy
  validates :name, presence: true

  after_create_commit { broadcast_prepend_to "rooms" }
end
