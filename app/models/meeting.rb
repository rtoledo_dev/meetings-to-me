# frozen_string_literal: true

class Meeting < ApplicationRecord
  default_scope { order(scheduled_at: :asc) }
  belongs_to :user
  belongs_to :room

  validates :title, :day, :scheduled_at, presence: true
  validates_inclusion_of :scheduled_at, in: 8..18
  validates :scheduled_at, uniqueness: true, if: :uniqueness_by_user?
  validates_associated :room

  def start_time
    day
  end

  protected

  def identical_meeting
    Room.joins(:meetings).where(
      "
        rooms.name = ? AND
        meetings.user_id != ? AND
        meetings.day = ? AND
        meetings.scheduled_at = ?",
      room.name,
      user.id,
      day,
      scheduled_at
    ).exists?
  end

  def uniqueness_by_user?
    if room.present? && user.present? && title.present? && day.present? && scheduled_at.present? && !identical_meeting
      false
    else
      true
    end
  end
end
