# frozen_string_literal: true

module DashboardHelper
  def short_meeting_title(meeting)
    "#{l(meeting.day, format: :short)} as #{meeting.scheduled_at}h - #{meeting.title.truncate(14)}"
  end

  def long_meeting_title(meeting)
    "Room: #{meeting.room.name} created by #{meeting.user.email} at #{l(
      meeting.day,
      format: :short
    )} as #{meeting.scheduled_at}h - #{meeting.title.truncate(14)}"
  end
end
