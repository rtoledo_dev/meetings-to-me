# frozen_string_literal: true

class MeetingsController < ApplicationController
  def index
    date_to_start = params[:start_date].blank? ? Date.today.beginning_of_month : params[:start_date].to_date.beginning_of_month
    date_to_end = date_to_start.end_of_month
    @meetings = Meeting.where('day between ? and ?', date_to_start, date_to_end).all
    @meeting = Meeting.new
  end

  def create
    @meeting = current_user.meetings.build(meeting_params)
    @meeting.save
    redirect_to(meetings_path)
  end

  def edit
    render(action: :index)
  end

  def update
    @meeting = current_user.meetings.find(params[:id])
    @meeting.update(meeting_params)

    redirect_to(meetings_path)
  end

  private

  def meeting_params
    params.require(:meeting).permit(:title, :room_id, :user_id, :day, :scheduled_at)
  end
end
