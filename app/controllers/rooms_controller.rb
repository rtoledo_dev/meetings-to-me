# frozen_string_literal: true

class RoomsController < ApplicationController
  before_action :set_room, :only_if_exists, only: %i[edit update destroy]
  before_action :set_rooms, only: %i[edit index]

  def index
    @room = Room.new
  end

  def create
    @room = Room.create(room_params)
    redirect_to(rooms_path)
  end

  def edit
    render(action: :index)
  end

  def update
    @room = Room.find(params[:id])
    @room.update(room_params)

    redirect_to(rooms_path)
  end

  def destroy
    @room.destroy
    redirect_to(rooms_path)
  end

  private

  def set_rooms
    @rooms = Room.all
  end

  def set_room
    @room = Room.find(params[:id])
  end

  def only_if_exists
    redirect_to(rooms_path) unless @room.present?
  end

  def room_params
    params.require(:room).permit(:name)
  end
end
