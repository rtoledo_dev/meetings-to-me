# frozen_string_literal: true

require 'faker'

[
  'rodrigo@rtoledo.inf.br',
  'main-user@meetings-to-me.com',
  'simple-user@meetings-to-me.com'
].map do |email|
  User.create!(email: email, password: 'asdqwe123', password_confirmation: 'asdqwe123')
end

User.all.each do |user|
  2.times do
    room = Room.create(name: Faker::Job.title)
    date_start_month = Date.today.beginning_of_month
    date_end_month = Date.today.end_of_month
    days = (date_end_month - date_start_month).to_i
    days.times.each do |i|
      day = date_start_month.change(day: i + 1)
      meeting = user.meetings.build(
        room_id: room.id,
        title: 'Daily Meeting',
        day: day,
        scheduled_at: 9 + user.id
      )
      meeting.save!
    end
  end
end
