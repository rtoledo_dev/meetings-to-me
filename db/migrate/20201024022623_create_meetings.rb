# frozen_string_literal: true

class CreateMeetings < ActiveRecord::Migration[6.0]
  def change
    create_table(:meetings) do |t|
      t.references(:user, null: false, foreign_key: true)
      t.references(:room, null: false, foreign_key: true)
      t.string(:title)
      t.date(:day)
      t.integer(:scheduled_at)

      t.timestamps
    end
  end
end
