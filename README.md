# Meetings to Me

This application was created to create rooms and meetings for users.

## Gems to take a look

- `rspec-rails` of course to test your code
- `simplecov` show you the code coverage
- `database_cleaner-active_record` and `database_cleaner-redis` to clean your database when steel coding with tests are running
- `redis`, `hotwire` to build server responses with actions dispatchs on frontend

## Development instructions

It's easy to start the development code or just run the application. Follown the instructions in your console assuming you have Docker

- ``brew install docker-compose``
- ``docker-compose build``
- ``docker-compose run web bundle exec rake webpacker:install``
- ``docker-compose run web bundle exec rails db:drop db:create db:migrate db:seed``
- ``docker-compose up``

Finally access ``http://localhost:3000``

## Clean code...

It's good everytime take a look in ``coverage`` directory, there you can open ``index.html`` and checkout how your code is over tests.

Anothoer point is run ``docker-compose run web rubocop --auto-correct-all`` if is possible. With this tool lot of tips takes notes to your code be the same in every place, conventions.

## Tests

Just run

- ``docker-compose run web bundle exec guard``
