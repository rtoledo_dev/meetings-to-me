# frozen_string_literal: true

FactoryBot.define do
  factory :meeting do
    room factory: :room
    user factory: :user
    title { Faker::Company.industry }
    day { '2020-10-14'.to_date }
    scheduled_at { 9 }
  end
end
