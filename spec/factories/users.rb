# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email { 'user@meetings-to-me.com' }
    password { 'asdqwe123' }
    password_confirmation { 'asdqwe123' }
  end
end
