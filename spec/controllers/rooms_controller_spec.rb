# frozen_string_literal: true

require 'rails_helper'

RSpec.describe(RoomsController, type: :controller) do
  login_user

  describe 'GET /index' do
    before do
      5.times do |i|
        create(:room, name: "Daily Meeting #{i}")
      end
    end

    it 'returns http success with correct data' do
      get :index
      expect(response).to(have_http_status(:success))
      expect(assigns(:rooms)).to(have(5).items)
    end
  end

  describe 'POST /create' do
    it 'Create with success' do
      expect { post(:create, params: { room: attributes_for(:room) }) }.to(change(Room, :count).by(1))
    end
  end

  describe 'PUT /update' do
    let!(:room) { create(:room) }
    it 'Update with success' do
      put :update, params: { id: room.id, room: attributes_for(:room, name: 'New name') }
      room.reload
      expect(room.name).to(eql('New name'))
    end
  end

  describe 'DESTROY /destroy' do
    let!(:room) { create(:room) }
    it 'Delete with success' do
      expect(Room.count).to(equal(1))
      delete :destroy, params: { id: room }
      expect(response).to(have_http_status(302))
      expect(Room.count).to(equal(0))
    end
  end
end
